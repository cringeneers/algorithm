# Базовые библиотеки
import pandas as pd
import numpy as np

# Парсинг координат
import requests
import xmltodict

# Деплоинг микросервиса
from flask import Flask, request, jsonify


# Сервис

app = Flask(__name__)


@app.route('/algorithm/', methods=['POST'])
def algorithm():

    data = request.get_json()

    # Делим json файл на два датафрейма
    point = pd.DataFrame(data['datasetDTOList'])
    point.drop(['officePointId', 'city'], axis=1 , inplace=True)
    point = point.rename(columns={"address": "Адрес точки, г. Краснодар",
                              "connectionDate": "Когда подключена точка?",
                              "materialsDelivered": "Карты и материалы доставлены?",
                              "daysSinceLastCardDelivery": "Кол-во дней после выдачи последней карты",
                              "approvedApplications": "Кол-во одобренных заявок",
                              "issuedCards": "Кол-во выданных карт"})

    worker = pd.DataFrame(data['employeeWithLocationDTOList'])
    worker = worker.rename(columns={"fullName": "ФИО",
                              "address": "Адрес локации",
                              "employeeLevel": "Грейд"})
    worker.drop('id', axis=1 , inplace=True)

    # Присвоение типа задачи к каждой точке
    conditions = [
        ((point['Кол-во дней после выдачи последней карты'] > 7) & (point['Кол-во одобренных заявок'] > 0)) | (
                    point['Кол-во дней после выдачи последней карты'] > 14),
        (point['Кол-во выданных карт'] / point['Кол-во одобренных заявок']) < 0.5,
        point['Когда подключена точка?'].eq('вчера') | point['Карты и материалы доставлены?'].eq(False)
    ]

    types = [1, 2, 3]
    time = [4, 2, 1.5]

    # Приоритет присваивать необязательно, у каждого типа задачи уникальный приоритет
    # Присваиваем тип и время
    point['type'] = np.select(conditions, types, default=0)
    point['time'] = np.select(conditions, time, default=0)

    point.drop(point.columns[[1, 2, 3, 4, 5]], axis=1, inplace=True)

    # Меняем значение типа у одного адреса по требованию организаторов
    point.at[6, 'type'] = 2

    # Собираем адреса офисов и точек вместе, чтобы спарсить матрицу времен
    point['Адрес точки, г. Краснодар'] = point['Адрес точки, г. Краснодар'].str.replace(r'[А-Я]\.[А-Я]\.', '',
                                                                                        regex=True)
    worker['Адрес локации'] = worker['Адрес локации'].str.replace(r'[А-Я]\.[А-Я]\.', '', regex=True)

    addresses_w = ('Краснодар, ' + worker['Адрес локации']).unique().tolist()
    addresses_p = ('Краснодар, ' + point['Адрес точки, г. Краснодар']).tolist()
    addresses_w.extend(addresses_p)

    # Парсим координаты, добавляем их к точкам и сотрудникам
    def geocode_address(address):
        api_key = "eb38efcd-1dda-4bd7-a099-061653eb33ab"  # Замените на свой API-ключ
        api_url = f"https://geocode-maps.yandex.ru/1.x/?apikey={api_key}&geocode={address}"

        response = requests.get(api_url)
        if response.status_code == 200:
            xml_data = response.text
            data_dict = xmltodict.parse(xml_data)
            pos_text = data_dict["ymaps"]["GeoObjectCollection"]["featureMember"]["GeoObject"]["Point"]["pos"]
            lon, lat = pos_text.split(" ")
            return {"address": address, "latitude": lat, "longitude": lon}

        return None

    addresses = addresses_w

    coordinates = []
    for address in addresses:
        result = geocode_address(address)
        if result:
            coordinates.append(result)

    coordinate = pd.DataFrame(coordinates)

    coordinate['address'] = coordinate['address'].str.replace('Краснодар, ', '', regex=True)

    coordinates = {
        "locations": coordinate[['latitude', 'longitude']].values.tolist()
    }

    coordinate['latitude'] = coordinate['latitude'].astype(str)
    coordinate['longitude'] = coordinate['longitude'].astype(str)

    worker = pd.merge(worker, coordinate, left_on='Адрес локации', right_on='address', how='left')

    worker['Координаты'] = '(' + worker['latitude'] + ', ' + worker['longitude'] + ')'

    # Удаление дополнительного столбца адреса
    worker.drop(['address', 'latitude', 'longitude'], axis=1, inplace=True)

    coordinate['address'] = coordinate['address'].str.replace('Краснодар, ', '', regex=True)
    point = pd.merge(point, coordinate, left_on='Адрес точки, г. Краснодар', right_on='address', how='left')
    point['Координаты'] = '(' + point['latitude'] + ', ' + point['longitude'] + ')'
    point.drop(['address', 'latitude', 'longitude'], axis=1, inplace=True)

    point.drop_duplicates(inplace=True, ignore_index=True)

    # Парсим с сайта OPS API матрицы времени пути от каждого из 3 офисов до точек в json файл
    body = coordinates

    headers = {
        'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
        'Authorization': '5b3ce3597851110001cf62487242a2a1e16f492489dc2116e375da3a',
        'Content-Type': 'application/json; charset=utf-8'
    }
    call = requests.post('https://api.openrouteservice.org/v2/matrix/driving-car', json=body, headers=headers)

    print(call.status_code, call.reason)

    call = call.json()
    durations = call['durations']
    durations = [[821.34 if element is None else element for element in row] for row in durations]
    durations = [[100000 if element == 0 else element for element in row] for row in durations]
    durations = pd.DataFrame(durations)
    durations /= 3600

    # Добавим к сотрудникам их номер офиса для начала отсчета в матрице времен
    worker['office'] = 0
    for i in range(len(worker)):
        for j in range(len(worker['Адрес локации'].unique())):
            if worker['Адрес локации'][i] == worker['Адрес локации'].unique()[j]:
                worker.at[i, 'office'] = j

    # Создаем словарь для файла в ответ на запрос
    dict_names = {}
    info = {}

    for name in worker['ФИО']:
        info = {}
        info['address'] = [worker.loc[worker['ФИО'] == name, 'Адрес локации'].values[0]]
        info['Grade'] = worker.loc[worker['ФИО'] == name, 'Грейд'].values[0]
        info['current_x'] = worker.loc[worker['ФИО'] == name, 'office'].values[0]
        info['total_x'] = [worker.loc[worker['ФИО'] == name, 'Координаты'].values[0]]
        info['total_time'] = 0
        info['total_task'] = []
        dict_names[name] = info.copy()

    # Представление алгоритма: 1. Проходимся по каждому сотруднику; 2.Делаем точкой отсчета его офис; 3. Ищем минимальное
    # время до точки учитывая приоритет; 4. После нахождения точки удаляем ее из списка, продолжаем искать  минимальное
    # время уже от новой точки; 5. Повторяем действия, пока не заполним лимит по времени

    task_ind = point[point['type'] == 3].index.tolist()  # Список индексов точек с задачей для синьора
    task_ind3 = [x + 3 for x in task_ind]  # Т.к. в матрице индексы точек смещены из за офисов прибавляем 3

    task_ind = point[point['type'] == 2].index.tolist()  # Для мидла
    task_ind2 = [x + 3 for x in task_ind]

    task_ind = point[point['type'] == 1].index.tolist()  # Для Джуна
    task_ind1 = [x + 3 for x in task_ind]

    for i in range(len(worker)):
        x0 = worker['office'][i]  # Начальная точка (офис)
        t_time = 0  # Задаем общее время для счетчика

        # Синьоры
        if worker['Грейд'][i] == 'Синьор':
            while (t_time + 4 <= 8) & (
                    len(task_ind3) != 0):  # Подбираем задачи, пока не заполним график + проверяем, остались ли
                t_time += 4  # свободные задачи
                next_x = durations.loc[task_ind3, [x0]].idxmin()[
                    x0]  # Ищем индекс точки, до которой быстрее всего добраться
                t_time += durations[next_x][x0]  # Счетчик работает
                if t_time > 8:
                    break
                x0 = next_x  # Присваиваем найденнную точку как начальную
                task_ind3.remove(x0)  # Удалим индекс точки из списка, чтобы не приехали дважды

                # Пополняем словарь имен
                dict_names[worker['ФИО'][i]]['address'].append(point['Адрес точки, г. Краснодар'][x0 - 3])
                dict_names[worker['ФИО'][i]]['total_x'].append(
                    point['Координаты'][x0 - 3])  # Вычитаем обратно, т.к. в таблице с коор-
                dict_names[worker['ФИО'][i]]['total_task'].append('Стимулирование задач')  # динатами нет смещения

            while (t_time + 2 <= 8) & (len(task_ind2) != 0):  # Добавляем синьору задачи грейдом пониже,
                t_time += 2  # если у него осталось свободное время или задача в 4 часа
                next_x = durations.loc[task_ind2, [x0]].idxmin()[x0]  # не уместилась в рабочий график
                t_time += durations[next_x][x0]
                if t_time > 8:
                    break
                x0 = next_x
                task_ind2.remove(x0)

                # Пополняем словарь имен
                dict_names[worker['ФИО'][i]]['address'].append(point['Адрес точки, г. Краснодар'][x0 - 3])
                dict_names[worker['ФИО'][i]]['total_x'].append(point['Координаты'][x0 - 3])
                dict_names[worker['ФИО'][i]]['total_task'].append('Обучение сотрудника')

            while (t_time + 1.5 <= 8) & (len(task_ind1) != 0):
                t_time += 1.5
                next_x = durations.loc[task_ind1, [x0]].idxmin()[x0]
                t_time += durations[next_x][x0]
                if t_time > 8:
                    break
                x0 = next_x
                task_ind1.remove(x0)

                # Пополняем словарь имен
                dict_names[worker['ФИО'][i]]['address'].append(point['Адрес точки, г. Краснодар'][x0 - 3])
                dict_names[worker['ФИО'][i]]['total_x'].append(point['Координаты'][x0 - 3])
                dict_names[worker['ФИО'][i]]['total_task'].append('Доставка карт')

        dict_names[worker['ФИО'][i]]['total_time'] = t_time

        # Мидлы
        if worker['Грейд'][i] == 'Мидл':
            while (t_time + 2 <= 8) & (len(task_ind2) != 0):
                t_time += 2
                next_x = durations.loc[task_ind2, [x0]].idxmin()[x0]
                t_time += durations[next_x][x0]
                if t_time > 8:
                    break
                x0 = next_x
                task_ind2.remove(x0)

                # Пополняем словарь имен
                dict_names[worker['ФИО'][i]]['address'].append(point['Адрес точки, г. Краснодар'][x0 - 3])
                dict_names[worker['ФИО'][i]]['total_x'].append(point['Координаты'][x0 - 3])
                dict_names[worker['ФИО'][i]]['total_task'].append('Обучение сотрудника')

            while (t_time + 1.5 <= 8) & (len(task_ind1) != 0):
                t_time += 1.5
                next_x = durations.loc[task_ind1, [x0]].idxmin()[x0]
                t_time += durations[next_x][x0]
                if t_time > 8:
                    break
                x0 = next_x
                task_ind1.remove(x0)

                # Пополняем словарь имен
                dict_names[worker['ФИО'][i]]['address'].append(point['Адрес точки, г. Краснодар'][x0 - 3])
                dict_names[worker['ФИО'][i]]['total_x'].append(point['Координаты'][x0 - 3])
                dict_names[worker['ФИО'][i]]['total_task'].append('Доставка карт')
        dict_names[worker['ФИО'][i]]['total_time'] = t_time

        # Джуны
        if worker['Грейд'][i] == 'Джун':
            while (t_time + 1.5 <= 8) & (len(task_ind1) != 0):
                t_time += 1.5
                next_x = durations.loc[task_ind1, [x0]].idxmin()[x0]
                t_time += durations[next_x][x0]
                if t_time > 8:
                    break
                x0 = next_x
                task_ind1.remove(x0)

                # Пополняем словарь имен
                dict_names[worker['ФИО'][i]]['address'].append(point['Адрес точки, г. Краснодар'][x0 - 3])
                dict_names[worker['ФИО'][i]]['total_x'].append(point['Координаты'][x0 - 3])
                dict_names[worker['ФИО'][i]]['total_task'].append('Доставка карт')
        dict_names[worker['ФИО'][i]]['total_time'] = t_time

    # Список невыполненных задач
    untask = []
    untask.extend(task_ind3)
    untask.extend(task_ind2)
    untask.extend(task_ind1)
    untask = [x - 3 for x in untask]

    # Поменяем тип данных, потому что json любит только такие
    for value in dict_names.values():
        value['current_x'] = int(value['current_x'])
        value['total_time'] = float(value['total_time'])

    return jsonify(dict_names)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
